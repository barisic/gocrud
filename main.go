package main

import (
	"fmt"
	"time"
	"database/sql"
	"log"
	_ "github.com/bmizerany/pq"
)

const DB_NAME = "mydb"

var Postgres int

var datetime = time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)

var engine = "PostgreSQL"

var dbs *sql.DB

var CREATE = map[int]string{
	Postgres: "CREATE TABLE times (id integer, datetime timestamp without time zone)",
}

var INSERT = fmt.Sprintf("INSERT INTO times (id, datetime) VALUES(0, '%s')",
	datetime.Format(time.RFC3339))

const (

	SELECT = "SELECT * FROM times WHERE id = 0"
)

// For SQL table "times"
type Times struct {
	Id       int
	Datetime time.Time
}

func (t Times) Args1() []interface{} {
	return []interface{}{&t.Id, &t.Datetime}
}

func (t Times) Args2() []interface{} {
	tt := t.Datetime.Format(time.RFC3339)
	return []interface{}{&t.Id, &tt}
}


func main() {

	log.SetFlags(0)

	username := "postgres"

	dbPostgres, err := sql.Open("postgres", fmt.Sprintf("user=%s password=%s dbname=%s host=%s sslmode=disable",
		username, "joh518008", DB_NAME, "/var/run/postgresql"))
	if err != nil {
		panic(err)
	}

	dbs = dbPostgres

	if _, err = dbPostgres.Exec(CREATE[Postgres]); err != nil {
		panic(err)
	}

	input := Times{0, datetime} // SQL statement in INSERT
	output := Times{}

	_, err = dbs.Exec(INSERT)
	if err != nil {
		log.Print(err)
	}

	rows := dbs.QueryRow(SELECT)
	if err = rows.Scan(output.Args1()...); err != nil {
		log.Printf("%s: %s\n\n", engine, err)
	} else {
		if fmt.Sprintf("%v", input) != fmt.Sprintf("%v", output) {
			log.Printf("%s: got different data\ninput:  %v\noutput: %v\n\n",
				engine, input, output)
		}
	}

	//dropTable("times")

	dbs.Close()
}

func dropTable(tableName string){

	dropQuery := fmt.Sprintf("DROP TABLE %s", tableName)

	_, err := dbs.Exec(dropQuery)
	if err != nil{
		log.Print(err)
	}
}


